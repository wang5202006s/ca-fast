<?php
use App\Core\App\CommandApp;
ini_set('date.timezone','Asia/Shanghai');

require_once __DIR__.'/../vendor/autoload.php';
$config = require_once __DIR__.'/../config/config.php';

if(empty($argv[1])){
    $argv[1] = 'hello/index';
}

try {
    $app = new CommandApp($config);
    $app->run($argv[1],$argv);
} catch (\Throwable $e) {
    echo $e->getMessage()."\n";
}

