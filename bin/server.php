<?php
use App\Core\App\SwooleHttpApp;

ini_set('date.timezone','Asia/Shanghai');

$func = isset($argv[1]) ? $argv[1] : 'start';
$funcArr = ['start','stop','reload'];
if(!in_array($func,$funcArr)){
    echo "指令不正确\ncommand ---> " . implode(' | ',$funcArr)." | 默认start\n";
    die;
}

require_once __DIR__.'/../vendor/autoload.php';
$config = require_once __DIR__.'/../config/config.php';
define('IS_DEV',$config['env']['is_debug']);
define('APP_CPU_NUM',swoole_cpu_num());

$app = new SwooleHttpApp($config);
$app->run($func);
