<?php
use App\Core\App\FpmApp;
use App\Core\Logs;
use App\Core\Responses\ResponseFpm;

ini_set('date.timezone','Asia/Shanghai');
require_once __DIR__.'/../vendor/autoload.php';
$config = require_once __DIR__.'/../config/config.php';
define('IS_DEV',$config['env']['is_debug']);

try{
    $app = new FpmApp($config);
    $app->run();
}catch (\Throwable $e){
    ResponseFpm::error(500,$e->getMessage());
    Logs::write($e->getMessage().'--'.$e->getFile().'--'.$e->getLine());
}