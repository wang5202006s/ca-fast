<?php
namespace App\Models;

use App\Core\Mysql\Model;

class Users extends Model
{
    public static string $db = 'db';

    public static string $table = 'users';
}
