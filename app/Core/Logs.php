<?php
namespace App\Core;

class Logs
{
    public static string $path = __DIR__ . '/../../runtime/';

    /**
     * 写入普通错误日志
     * @param string $msg
     */
    public static function write(string $msg)
    {
        $file = self::$path . 'error_' . date("Ymd") . '.log';
        $msg = date('Y-m-d H:i:s') . '----' . $msg ."\r\n";
        file_put_contents($file, $msg, FILE_APPEND);
    }
}