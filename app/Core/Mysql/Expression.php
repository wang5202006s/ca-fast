<?php
namespace App\Core\Mysql;

class Expression
{
    public string $sql = '';

    /**
     * Expression constructor.
     * @param string $sql
     */
    public function __construct(string $sql)
    {
        $this->sql = $sql;
    }
}
