<?php
namespace App\Core\Mysql;
use Exception;

class Model
{
    /**
     * @var string
     */
    public static string $table = '';

    /**
     * @var string
     */
    public static string $db = 'db';

    /**
     * @return Query
     */
    public static function find()
    {
        return Query::find()->table(static::$table)->db(static::$db);
    }

    /**
     * @throws Exception
     */
    public static function begin()
    {
        Db::begin(static::$db);
    }

    /**
     * @throws Exception
     */
    public static function commit()
    {
        Db::commit(static::$db);
    }

    /**
     * @throws Exception
     */
    public static function back()
    {
        Db::back(static::$db);
    }
}
