<?php
namespace App\Core;

class Container
{
    public static array $context = [];

    /**
     * @param $key
     * @param $value
     */
    public static function set($key,$value)
    {
        static::$context[$key] = $value;
    }

    /**
     * @param $key
     * @return mixed|null
     */
    public static function get($key)
    {
        if(isset(static::$context[$key])){
            return static::$context[$key];
        }
        return null;
    }

    public static function clean($key)
    {
        static::$context[$key] = null;
        unset(static::$context[$key]);
    }
}