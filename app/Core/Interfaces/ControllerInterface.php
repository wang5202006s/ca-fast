<?php
namespace App\Core\Interfaces;

interface ControllerInterface
{
    public function before():bool;
}
