<?php
namespace App\Core\Responses;

class Response
{
    /**
     * @param int $status
     * @param string $msg
     * @param mixed $data
     * @return false
     */
    public static function error(int $status = 500,string $msg = '',$data = [])
    {
        $data = [
            'status' => $status,
            'data'   => $data,
            'msg'    => $msg
        ];
        self::out($data);
        return false;
    }

    /**
     * @param mixed $data
     * @param string $msg
     * @return false
     */
    public static function success($data = [],string $msg = 'success')
    {
        $data = [
            'status' => 0,
            'data'   => $data,
            'msg'    => $msg
        ];
        self::out($data);
        return false;
    }

    public static function out(array $data)
    {
        if(response()->isWritable()){
            response()->header('Content-type','application/json; charset=utf-8');
            response()->end(json_encode($data));
        }
    }
}

