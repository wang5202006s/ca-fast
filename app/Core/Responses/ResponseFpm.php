<?php
namespace App\Core\Responses;

class ResponseFpm
{
    /**
     * @param int $code 状态码 说明 200正常 500系统异常 400需要登录 1参数错误 2内部错误
     * @param string $msg
     * @param array $data
     * @return false
     */
    public static function error($code = 500,$msg = 'error',$data = [])
    {
        $data = [
            'code' => $code,
            'data' => $data,
            'msg'  => $msg
        ];
        return self::echoData($data);
    }

    public static function success($data = [],$msg = 'success')
    {
        $data = [
            'code' => 200,
            'data' => $data,
            'msg'  => $msg
        ];
        return self::echoData($data);
    }

    public static function echoData(array $data)
    {
        header('Content-type:application/json;charset=UTF-8');
        echo json_encode($data);
        return false;
    }
}

