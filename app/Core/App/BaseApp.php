<?php
namespace App\Core\App;

class BaseApp
{
    public array $config = [];
    public string $defaultController = 'index';
    public string $defaultAction = 'test';

    public function __construct(array $config)
    {
        $this->config = $config;
    }
}
