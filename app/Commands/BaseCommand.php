<?php
namespace App\Commands;

use App\Core\Interfaces\ControllerInterface;

class BaseCommand implements ControllerInterface
{
    public array $input = [];

    public function before():bool
    {
        $this->input = func_get_args();
        return true;
    }
}
