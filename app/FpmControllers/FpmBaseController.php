<?php
namespace App\FpmControllers;
use App\Core\Interfaces\ControllerInterface;

class FpmBaseController implements ControllerInterface
{
    public function before(): bool
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Methods: *');
        if($_SERVER['REQUEST_METHOD'] == 'OPTIONS'){
            return false;
        }
        return true;
    }
}