<?php
namespace App\Controllers;
use App\Core\Interfaces\ControllerInterface;

class BaseController implements ControllerInterface
{
    public function before(): bool
    {
        response()->header('Access-Control-Allow-Origin','*');
        response()->header('Access-Control-Allow-Headers','*');
        response()->header('Access-Control-Allow-Methods','*');
        $server = request()->server;
        if($server['request_method'] == 'OPTIONS'){
            return false;
        }
        return true;
    }
}