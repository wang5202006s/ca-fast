<?php
use App\Core\App\SwooleHttpApp;
use App\Core\Container;
use App\Core\Redis\Db;

/**
 * @return SwooleHttpApp
 */
function server(){
    return Container::get('server');
}

/**
 * @return Swoole\Http\Response
 */
function response(){
    return Container::get('response');
}

/**
 * @return Swoole\Http\Request
 */
function request(){
    return Container::get('request');
}

/**
 * @return array
 */
function config(){
    return server()->config;
}

/**
 * @return array
 */
function params(){
    $config = config();
    return $config['params'];
}

/**
 * @param string $key
 * @return Redis
 * @throws Exception
 */
function redis($key = 'redis'){
    return Db::getRedis($key);
}
