<?php
return [
    'params' => require_once __DIR__.'params.php',
    'db' => require_once __DIR__.'db.php',
    'redis' => require_once __DIR__.'redis.php',
    'env'   => require_once __DIR__.'env.php',
    'server' => [
        'host'      => '127.0.0.1',
        'port'      => 99,
        'is_base'   => false,
        'set'       => [
            'reactor_num'       => swoole_cpu_num(),
            'worker_num'        => swoole_cpu_num(),
            'daemonize'         => false,
            'enable_reuse_port' => true,
            'log_file'          => __DIR__.'/../runtime/server/server.log',
            'log_level'         => SWOOLE_LOG_NOTICE,
            'log_rotation'      => SWOOLE_LOG_ROTATION_DAILY
        ]
    ]
];

