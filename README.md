# 使用文档  
## 运行环境
* php >= 7.4  
* swoole >= 4.8  
## 依赖扩展
* PDO  
* redis  
* swoole  

## 安装
````
composer create-project wang5202006s/ca-fast
````
## 配置运行环境
````
cd ./ca-fast
mkdir ./runtime
chmod 777 -R ./runtime
````
## 启停Swoole HTTP服务
````
启动 php ./bin/server.php 或 php ./bin/server.php start
停止 php ./bin/server.php stop
重启 php ./bin/server.php reload
````
## 框架描述
````
1、一个极简的框架，轻松简单，无任何负担直接可用。
2、框架没有什么中间件，路由概念。也没有什么AOP，注解、验证器、ORM，简单的高效的框架无需这些东西。
3、框架当前不支持协程，请勿开启协程配置或hook。
4、遵循psr4。
````

## 使用
### 控制器
* 所有控制器有 控制器(controller)名与方法(action)两部分组成  
* 路径格式 controller/action  
* 所有控制都需实现App\Core\Interfaces\ControllerInterface 的控制器接口  
* before方法为在调用action之前必须调用的，当before方法返回true时才会继续执行action  
* Commands控制器无论before返回什么都会继续执行action  
* 控制器的三个目录  
````
1、Controllers （Swoole HTTP 服务使用的控制器目录）
路径规则举栗说明：
请求地址为：http://127.0.0.1:99/index/test
将会调用 控制器目录下 IndexController.php(IndexController类) 下的 actionTest方法。
这里的index表示需要调用的控制器 test表示需要执行的类方法

2、Commands  (指令运行的控制器目录)
路径规则举栗说明：
php ./bin/cmd.php hello/index
将会调用 控制器目录下 HelloController.php(HelloController类) 下的 index方法。
这与上方不一致的地方就是 调用的方法无需在方法名前添加action标识。

3、FpmControllers (PHP-FPM HTTP 服务使用的控制器目录)
路径规则举栗说明：同Swoole HTTP
````

### MYSQL
#### 配置连接信息  
````
在config/config.php中添加一个数据库
config.php配置栗子：
<php
return [
    'db' => [       //配置一个名称叫db的MYSQL配置 名称请勿与其他配置重名
        'dsn' => 'mysql:host=127.0.0.1;dbname=test;charset=utf8mb4',
        'username' => 'root',
        'password' => '123456'
    ],
    'db2' => [      //配置一个名称叫db2的MYSQL配置 名称请勿与其他配置重名
        'dsn' => 'mysql:host=127.0.0.1;dbname=api;charset=utf8mb4',
        'username' => 'test',
        'password' => '654321'
    ]
];
````
#### Db类  
##### 描述
* 获取到的连接是可用的，断线后框架将自动进行重连。  
* 参数传递采用占位符方式。见下方栗子  
````
$sql = "select * from user where nick like ? or username = ?";
$params = ['%张三%','张三'];
$users = Db::queryAll($sql,$params,'db');
````
##### 方法
* getDb('db'); 获取一个数据库连接 这里的db为配置中的名称，如果名称不存在会抛出异常。  
* queryOne(string $sql,array $params = [],$key = 'db') 查询并只返回一条数据   
* queryAll(string $sql,array $params = [],$key = 'db') 查询并返回所有数据  
* query(string $sql,array $params = [],$key = 'db') 查询并返回Statement  
* exec(string $sql,array $params = [],$key = 'db') 执行并返回受影响行数  
* getLastId($key = 'db')  获取上一次插入的主键ID
* begin($key = 'db')  开启事务
* commit($key = 'db') 提交
* back($key = 'db') 回滚
* connect($key = 'db')  通过配置名称 创建一个连接  
* ping(PDO $connect) 检测连接是否可用  
* 其他PDO操作，可调用getDb方返回PDO对象，可随意使用PDO  

##### 属性
* $connectArr 连接数组，保存所有名称的连接。  
* $pingTime   自动ping间隔时间，设置成0表示每次调用getDb方法时都会检测是否可用。  

#### query类
##### 描述
* 简易的查询构造器  
* 所有输入的值，都会自动转换为占位符(除开Expression中的值)  
* 不支持直接构造join、or等复杂的SQL  
* 复杂的可通过 new Expression(SQL) 处理。见下方栗子  
````
$users = Query::find()->table('user')
->select('id,nick,sex,age')
->where(['sex' => 1])
->andWhere(['>=','age',20])
->andWhere(new Expression("nick like '%张三%' or username = '张三'"))
->all();
````
##### 使用方法
* 查询
````
$user = Query::find->table('user')
->select(['id','nick','sex','age'])
->where(['id' => 1,'status' => 0])
->one();

$users = Query::find->table('user')
->select(['id','nick','sex','age'])
->where(['id' => 1,'status' => 0])
->all();
````

